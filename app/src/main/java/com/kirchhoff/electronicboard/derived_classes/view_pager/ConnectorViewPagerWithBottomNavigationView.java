package com.kirchhoff.electronicboard.derived_classes.view_pager;

import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ConnectorViewPagerWithBottomNavigationView {

    private final WeakReference<FragmentManager> fragmentManagerRef;
    private final WeakReference<BottomNavigationView> bottomNavigationViewRef;
    private final WeakReference<ViewPager> viewPagerRef;

    private List<Integer> keys;
    private List<Class<? extends Fragment>> values;

    public ConnectorViewPagerWithBottomNavigationView(
            FragmentManager fragmentManager,
            BottomNavigationView bottomNavigationView,
            ViewPager viewPager) {

        this.fragmentManagerRef = new WeakReference<>(fragmentManager);
        this.bottomNavigationViewRef = new WeakReference<>(bottomNavigationView);
        this.viewPagerRef = new WeakReference<>(viewPager);
    }

    // Integer is a menuItemId. It uses as a key for fragments.
    public void connect(final LinkedHashMap<Integer, Class<? extends Fragment>> fragmentsLinkedHashMap) {

        keys = new ArrayList<>(fragmentsLinkedHashMap.keySet());
        values = new ArrayList<>(fragmentsLinkedHashMap.values());

        PagerAdapter adapter = new FragmentStatePagerAdapter(fragmentManagerRef.get()) {
            @Override
            public int getCount() {
                return keys.size();
            }

            @Override
            public Fragment getItem(int position) {

                Class<?> fragmentClass = values.get(position);
                Constructor<?> constructor = fragmentClass.getConstructors()[0];

                try {
                    return (Fragment) constructor.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

                return null;
            }
        };

        viewPagerRef.get().setAdapter(adapter);

        bottomNavigationViewRef.get().setOnNavigationItemSelectedListener(item -> {
            int itemId = item.getItemId();
            int selectedPosition = keys.indexOf(itemId);

            if (selectedPosition == -1) {
                return false;
            }

            viewPagerRef.get().setCurrentItem(selectedPosition);
            return true;
        });

        // when a swipe detected in the view pager, we update BottomNavigationView
        viewPagerRef.get().addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                int itemId = keys.get(position);
                bottomNavigationViewRef.get().setSelectedItemId(itemId);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }
}