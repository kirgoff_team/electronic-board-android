package com.kirchhoff.electronicboard.derived_classes.view_pager;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class ConnectorViewPagerWithTabLayout {
    private final FragmentManager fragmentManager;
    private final TabLayout tabLayout;
    private final ViewPager viewPager;

    public ConnectorViewPagerWithTabLayout(FragmentManager fragmentManager,
                                           TabLayout tabLayout,
                                           ViewPager viewPager) {
        this.tabLayout = tabLayout;
        this.viewPager = viewPager;
        this.fragmentManager = fragmentManager;
    }

    public void connect(final List<Class<? extends Fragment>> fragments) {

        PagerAdapter adapter = new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {

                Class<?> fragmentClass = fragments.get(position);
                Constructor<?> constructor = fragmentClass.getConstructors()[0];

                try {
                    return (Fragment) constructor.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }

                return null;
            }
        };

        viewPager.setAdapter(adapter);

        // when a swipe detected in the view pager, we update current tab
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {}

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {}
                }
        );
    }
}
