package com.kirchhoff.electronicboard.activities.login.registration.data;

public class RegistrationDtoRequest {
    public String login;
    public String password;

    public RegistrationDtoRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }
}