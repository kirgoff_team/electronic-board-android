package com.kirchhoff.electronicboard.activities.base.architecture_components;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.kirchhoff.electronicboard.activities.base.BaseFragment;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;

import javax.inject.Inject;

public class BaseViewFragment<P extends BaseContract.VPContract.Presenter>
        extends BaseFragment
        implements BaseContract.VPContract.View {

    protected P presenter;

    @Inject
    public void injectPresenter(P presenter) {
        this.presenter = presenter;
        presenter.setView(this);
    }

    @Override
    public void onDetach() {
        presenter.onDestroy();
        super.onDetach();
    }

    //region Implementation: BaseContract.VPContract.View
    @Override
    public void onErrorMessage(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorInvalidRequestPayload() {
        onErrorMessage("Invalid Request Payload");
    }

    @Override
    public void onErrorBadToken() {
        onErrorMessage("Bad Token");
    }

    @Override
    public void onErrorPermissionDenied() {
        onErrorMessage("Permission Denied");
    }

    @Override
    public void onErrorRequestingUserNotFound() {
        onErrorMessage("Requesting User Not Found");
    }

    @Override
    public void onErrorInternalServerError() {
        onErrorMessage("Internal Server Error");
    }
    //endregion
}