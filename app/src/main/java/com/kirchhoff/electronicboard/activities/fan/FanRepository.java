package com.kirchhoff.electronicboard.activities.fan;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseRepository;
import com.kirchhoff.electronicboard.repository.service.ServiceCallHub;

import javax.inject.Inject;

public class FanRepository
        extends BaseRepository<FanContract.IRContract.Interactor>
        implements FanContract.IRContract.Repository {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = FanRepository.class.getSimpleName();

    private ServiceCallHub serviceCallHub;

    @Inject
    FanRepository(ServiceCallHub serviceCallHub) {
        this.serviceCallHub = serviceCallHub;
    }

    //region Implementation: FanContract.IRContract.Repository
    //endregion
}