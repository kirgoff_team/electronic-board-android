package com.kirchhoff.electronicboard.activities.base.architecture_components;

import io.reactivex.disposables.CompositeDisposable;

public class BaseRepository<I extends BaseContract.IRContract.Interactor>
        implements BaseContract.IRContract.Repository {

    protected I interactor;
    protected CompositeDisposable disposables = new CompositeDisposable();

    @Override
    @SuppressWarnings("unchecked")
    public void setInteractor(BaseContract.IRContract.Interactor interactor) {
        this.interactor = (I) interactor;
    }

    @Override
    public void onDestroy() {
        disposables.dispose();
    }
}