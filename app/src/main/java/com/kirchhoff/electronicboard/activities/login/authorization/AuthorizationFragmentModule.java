package com.kirchhoff.electronicboard.activities.login.authorization;

import com.kirchhoff.electronicboard.activities.scopes.FragmentScope;

import dagger.Binds;
import dagger.Module;

@Module
public interface AuthorizationFragmentModule {

    @Binds
    @FragmentScope
    AuthorizationContract.VPContract.View provideAuthorizationContractVPContractView(AuthorizationFragment view);

    @Binds
    @FragmentScope
    AuthorizationContract.VPContract.Presenter provideAuthorizationContractVPContractPresenter(AuthorizationPresenter presenter);

    @Binds
    @FragmentScope
    AuthorizationContract.PIContract.Presenter provideAuthorizationContractPIContractPresenter(AuthorizationPresenter presenter);

    @Binds
    @FragmentScope
    AuthorizationContract.PIContract.Interactor provideAuthorizationContractPIContractInteractor(AuthorizationInteractor interactor);

    @Binds
    @FragmentScope
    AuthorizationContract.IRContract.Interactor provideAuthorizationContractIRContractInteractor(AuthorizationInteractor interactor);

    @Binds
    @FragmentScope
    AuthorizationContract.IRContract.Repository provideAuthorizationContractIRContractRepository(AuthorizationRepository repository);
}