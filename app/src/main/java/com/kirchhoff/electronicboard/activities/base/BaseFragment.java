package com.kirchhoff.electronicboard.activities.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kirchhoff.electronicboard.navigation.NavigationRouter;
import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

public class BaseFragment
        extends Fragment {

    @Inject
    protected RefWatcher leakCanaryRefWatcher;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        leakCanaryRefWatcher.watch(this);
    }
}
