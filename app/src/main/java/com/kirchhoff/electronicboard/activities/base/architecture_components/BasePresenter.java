package com.kirchhoff.electronicboard.activities.base.architecture_components;

import javax.inject.Inject;

public class BasePresenter <V extends BaseContract.VPContract.View, I extends BaseContract.PIContract.Interactor>
        implements BaseContract.PIContract.Presenter, BaseContract.VPContract.Presenter {

    protected V view;
    protected I interactor;

    @Inject
    public void injectPresenter(I interactor) {
        this.interactor = interactor;
        interactor.setPresenter(this);
    }

    //region Implementation: BaseContract.VPContract.Presenter
    @Override
    @SuppressWarnings("unchecked")
    public void setView(BaseContract.VPContract.View view) {
        this.view = (V) view;
    }

    @Override
    public void onDestroy() {
        interactor.onDestroy();
    }
    //endregion

    //region Implementation: BaseContract.PIContract.Presenter
    @Override
    public void onErrorMessage(String error) {
        view.onErrorMessage(error);
    }

    @Override
    public void onErrorInvalidRequestPayload() {
        view.onErrorInvalidRequestPayload();
    }

    @Override
    public void onErrorBadToken() {
        view.onErrorBadToken();
    }

    @Override
    public void onErrorPermissionDenied() {
        view.onErrorPermissionDenied();
    }

    @Override
    public void onErrorRequestingUserNotFound() {
        view.onErrorRequestingUserNotFound();
    }

    @Override
    public void onErrorInternalServerError() {
        view.onErrorInternalServerError();
    }
    //endregion
}