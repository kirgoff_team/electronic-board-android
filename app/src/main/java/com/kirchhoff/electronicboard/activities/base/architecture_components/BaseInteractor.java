package com.kirchhoff.electronicboard.activities.base.architecture_components;

import javax.inject.Inject;

public class BaseInteractor <R extends BaseContract.IRContract.Repository,
                             P extends BaseContract.PIContract.Presenter>
        implements BaseContract.IRContract.Interactor, BaseContract.PIContract.Interactor {

    protected R repository;
    protected P presenter;

    @Inject
    public void injectInteractor(R repository) {
        this.repository = repository;
        repository.setInteractor(this);
    }

    //region Implementation: BaseContract.PIContract.Interactor
    @Override
    @SuppressWarnings("unchecked")
    public void setPresenter(BaseContract.PIContract.Presenter presenter) {
        this.presenter = (P) presenter;
    }

    @Override
    public void onDestroy() {
        repository.onDestroy();
    }
    //endregion

    //region Implementation: BaseContract.IRContract.Interactor
    @Override
    public void onErrorMessage(String error) {
        presenter.onErrorMessage(error);
    }

    @Override
    public void onErrorInvalidRequestPayload() {
        presenter.onErrorInvalidRequestPayload();
    }

    @Override
    public void onErrorBadToken() {
        presenter.onErrorBadToken();
    }

    @Override
    public void onErrorPermissionDenied() {
        presenter.onErrorPermissionDenied();
    }

    @Override
    public void onErrorRequestingUserNotFound() {
        presenter.onErrorRequestingUserNotFound();
    }

    @Override
    public void onErrorInternalServerError() {
        presenter.onErrorInternalServerError();
    }
    //endregion
}