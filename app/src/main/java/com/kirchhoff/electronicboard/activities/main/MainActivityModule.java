package com.kirchhoff.electronicboard.activities.main;

import com.kirchhoff.electronicboard.activities.scopes.ActivityScope;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    @ActivityScope
    NavigationRouter injectNavigationRouter(MainActivity activity) {
        return new NavigationRouter(activity);
    }
}