package com.kirchhoff.electronicboard.activities.base;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseContract;
import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseViewFragment;

public abstract class LockableFragment<P extends BaseContract.VPContract.Presenter, VM, RI>
        extends BaseViewFragment<P> {

    public LockableFragment() {
        // Required empty public constructor
    }

    protected void sendRequest(@NonNull VM viewModel) {
        hideAllErrors();

        if (viewModelHasBadFields(viewModel)) {
            return;
        }

        showProgress(true);

        request(convertViewModelToRequest(viewModel));
    }

    abstract protected void request(@NonNull RI requestItem);

    abstract protected void hideAllErrors();

    abstract protected void showProgress(boolean show);

    abstract protected boolean viewModelHasBadFields(@NonNull VM viewModel);

    abstract protected RI convertViewModelToRequest(@NonNull VM viewModel);
}