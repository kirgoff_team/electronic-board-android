package com.kirchhoff.electronicboard.activities.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.kirchhoff.electronicboard.R;
import com.kirchhoff.electronicboard.activities.base.BaseActivity;
import com.kirchhoff.electronicboard.activities.fan.FanContract;
import com.kirchhoff.electronicboard.activities.fan.FanFragment;
import com.kirchhoff.electronicboard.activities.lights.LightsContract;
import com.kirchhoff.electronicboard.activities.lights.LightsFragment;
import com.kirchhoff.electronicboard.activities.sensors.SensorsContract;
import com.kirchhoff.electronicboard.activities.sensors.SensorsFragment;
import com.kirchhoff.electronicboard.databinding.ActivityMainBinding;
import com.kirchhoff.electronicboard.derived_classes.view_pager.ConnectorViewPagerWithBottomNavigationView;
import com.kirchhoff.electronicboard.utilities.StatusBarUtils;
import com.kirchhoff.electronicboard.utilities.ToolBarUtils;

import java.util.LinkedHashMap;

import dagger.android.AndroidInjection;

public class MainActivity
        extends BaseActivity
        implements
            SensorsContract.ViewListener,
            LightsContract.ViewListener,
            FanContract.ViewListener {

    @SuppressWarnings("FieldCanBeLocal")
    private ActivityMainBinding activityBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        //region Toolbar
        ToolBarUtils.setToolbarAsActionBar(this, activityBinding.toolbarMainActivity);
        ToolBarUtils.setDisplayHomeAsUpEnabled(this, false);
        ToolBarUtils.setTitle(this, R.string.app_name);
        //endregion

        //region Status Bar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion

        //region BottomNavigationView With ViewPager
        ConnectorViewPagerWithBottomNavigationView connectorViewPagerWithBottomNavigationView =
                new ConnectorViewPagerWithBottomNavigationView(
                        getSupportFragmentManager(),
                        activityBinding.bottomNavigationViewMainActivity,
                        activityBinding.viewPagerMainActivity
                );

        LinkedHashMap<Integer, Class<? extends Fragment>> fragmentsLinkedHashMap = new LinkedHashMap<>();

        fragmentsLinkedHashMap.put(R.id.navigation_sensors, SensorsFragment.class);
        fragmentsLinkedHashMap.put(R.id.navigation_lights, LightsFragment.class);
        fragmentsLinkedHashMap.put(R.id.navigation_fan, FanFragment.class);

        connectorViewPagerWithBottomNavigationView.connect(fragmentsLinkedHashMap);
        //endregion
    }
}