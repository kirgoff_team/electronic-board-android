package com.kirchhoff.electronicboard.activities.sensors;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseRepository;
import com.kirchhoff.electronicboard.repository.service.ServiceCallHub;

import javax.inject.Inject;

public class SensorsRepository
        extends BaseRepository<SensorsContract.IRContract.Interactor>
        implements SensorsContract.IRContract.Repository {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = SensorsRepository.class.getSimpleName();

    private ServiceCallHub serviceCallHub;

    @Inject
    SensorsRepository(ServiceCallHub serviceCallHub) {
        this.serviceCallHub = serviceCallHub;
    }

    //region Implementation: SensorsContract.IRContract.Repository
    //endregion
}