package com.kirchhoff.electronicboard.activities.lights;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseInteractor;

import javax.inject.Inject;

public class LightsInteractor
        extends BaseInteractor<LightsContract.IRContract.Repository, LightsContract.PIContract.Presenter>
        implements LightsContract.IRContract.Interactor, LightsContract.PIContract.Interactor {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = LightsInteractor.class.getSimpleName();

    @Inject
    LightsInteractor() {}

    //region Implementation: LightsContract.PIContract.Interactor
    //endregion

    // region Implementation: LightsContract.IRContract.Interactor
    //endregion
}
