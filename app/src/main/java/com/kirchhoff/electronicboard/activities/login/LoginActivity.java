package com.kirchhoff.electronicboard.activities.login;

import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.kirchhoff.electronicboard.R;
import com.kirchhoff.electronicboard.activities.base.BaseActivity;
import com.kirchhoff.electronicboard.activities.login.authorization.AuthorizationContract;
import com.kirchhoff.electronicboard.activities.login.authorization.AuthorizationFragment;
import com.kirchhoff.electronicboard.activities.login.registration.RegistrationContract;
import com.kirchhoff.electronicboard.activities.login.registration.RegistrationFragment;
import com.kirchhoff.electronicboard.activities.main.MainActivity;
import com.kirchhoff.electronicboard.databinding.ActivityLoginBinding;
import com.kirchhoff.electronicboard.derived_classes.view_pager.ConnectorViewPagerWithTabLayout;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;
import com.kirchhoff.electronicboard.utilities.StatusBarUtils;
import com.kirchhoff.electronicboard.utilities.ToolBarUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class LoginActivity extends BaseActivity
        implements RegistrationContract.ViewListener, AuthorizationContract.ViewListener {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = LoginActivity.class.getSimpleName();

    @SuppressWarnings("FieldCanBeLocal")
    private ActivityLoginBinding activityBinding;

    @Inject
    protected NavigationRouter navigationRouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        //region Toolbar
        ToolBarUtils.setToolbarAsActionBar(this, activityBinding.toolbarLoginActivity);
        ToolBarUtils.setDisplayHomeAsUpEnabled(this, false);
        ToolBarUtils.setTitle(this, R.string.login_activity_title_text);
        //endregion

        //region Status Bar
        StatusBarUtils.setStatusBarTranslucent(this, true);
        //endregion

        //region TabLayout with ViewPager
        ConnectorViewPagerWithTabLayout connectorViewPagerWithTabLayout =
                new ConnectorViewPagerWithTabLayout(
                        getSupportFragmentManager(),
                        activityBinding.tabLayoutLoginActivity,
                        activityBinding.viewPagerLoginActivity
                );

        List<Class<? extends Fragment>> fragments = new ArrayList<>();
        fragments.add(AuthorizationFragment.class);
        fragments.add(RegistrationFragment.class);

        connectorViewPagerWithTabLayout.connect(fragments);
        //endregion
    }

    //region Implementation: AuthorizationContract.ViewListener
    @Override
    public void onSuccessAuthorization() {

    }

    @Override
    public void onFailureAuthorization() {

    }
    //endregion

    //region Implementation: RegistrationContract.ViewListener
    @Override
    public void onSuccessRegistration() {
        navigationRouter.navigateTo(MainActivity.class);
    }

    @Override
    public void onFailureRegistration() {

    }
    //endregion
}
