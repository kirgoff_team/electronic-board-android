package com.kirchhoff.electronicboard.activities.login.authorization.data;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

public class AuthorizationViewModel extends BaseObservable {
    private String authLogin;
    private String authPassword;

    public AuthorizationViewModel() {}

    public AuthorizationViewModel(String login, String password) {
        this.authLogin = login;
        this.authPassword = password;
    }

    @Bindable
    public String getLogin() {
        return authLogin;
    }

    @Bindable
    public String getPassword() {
        return authPassword;
    }

    public void setLogin(String login) {
        this.authLogin = login;
        notifyPropertyChanged(BR.login);
    }

    public void setPassword(String password) {
        this.authPassword = password;
        notifyPropertyChanged(BR.password);
    }

    @Override
    public String toString() {
        return "AuthorizationViewModel{" +
                "authLogin='" + authLogin + '\'' +
                ", authPassword='" + authPassword + '\'' +
                '}';
    }
}