package com.kirchhoff.electronicboard.activities.login.authorization;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BasePresenter;
import com.kirchhoff.electronicboard.activities.login.authorization.data.AuthorizationDtoRequest;

import javax.inject.Inject;

public class AuthorizationPresenter
        extends BasePresenter<AuthorizationContract.VPContract.View, AuthorizationContract.PIContract.Interactor>
        implements AuthorizationContract.VPContract.Presenter, AuthorizationContract.PIContract.Presenter {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = AuthorizationPresenter.class.getSimpleName();

    @Inject
    AuthorizationPresenter() {}

    //region Implementation: AuthorizationContract.VPContract.Presenter
    @Override
    public void signIn(@NonNull AuthorizationDtoRequest request) {

    }
    //endregion

    //region Implementation: AuthorizationContract.PIContract.Presenter
    @Override
    public void onLoginError(String error) {

    }

    @Override
    public void onPasswordError(String error) {

    }

    @Override
    public void onSuccessAuthorization() {

    }

    @Override
    public void onFailureAuthorization() {

    }
    //endregion
}
