package com.kirchhoff.electronicboard.activities.login.registration;

import com.kirchhoff.electronicboard.activities.scopes.FragmentScope;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(includes = RegistrationFragmentModule.Declarations.class)
public final class RegistrationFragmentModule {

    @Provides
    @FragmentScope
    NavigationRouter provideNavigationRouter(RegistrationFragment view) {
        // Note: view.getContext() never be null because an inject()
        // calls in a fragment onAttach(Context context) method.
        // So we set noinspection ->
        // noinspection ConstantConditions
        return new NavigationRouter(view.getContext());
    }

    @Module
    public interface Declarations {
        @Binds
        @FragmentScope
        RegistrationContract.VPContract.View provideRegistrationContracttVPContractView(RegistrationFragment view);

        @Binds
        @FragmentScope
        RegistrationContract.VPContract.Presenter provideRegistrationContractVPContractPresenter(RegistrationPresenter presenter);

        @Binds
        @FragmentScope
        RegistrationContract.PIContract.Presenter provideRegistrationContractPIContractPresenter(RegistrationPresenter presenter);

        @Binds
        @FragmentScope
        RegistrationContract.PIContract.Interactor provideRegistrationContractPIContractInteractor(RegistrationInteractor interactor);

        @Binds
        @FragmentScope
        RegistrationContract.IRContract.Interactor provideRegistrationContractIRContractInteractor(RegistrationInteractor interactor);

        @Binds
        @FragmentScope
        RegistrationContract.IRContract.Repository provideRegistrationContractIRContractRepository(RegistrationRepository repository);
    }
}