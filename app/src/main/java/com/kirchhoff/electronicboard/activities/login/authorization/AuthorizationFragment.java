package com.kirchhoff.electronicboard.activities.login.authorization;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kirchhoff.electronicboard.R;
import com.kirchhoff.electronicboard.activities.base.LockableFragment;
import com.kirchhoff.electronicboard.activities.login.authorization.data.AuthorizationDtoRequest;
import com.kirchhoff.electronicboard.activities.login.authorization.data.AuthorizationViewModel;
import com.kirchhoff.electronicboard.databinding.FragmentAuthorizationBinding;
import com.kirchhoff.electronicboard.utilities.ViewUtils;

import dagger.android.support.AndroidSupportInjection;

public class AuthorizationFragment
        extends LockableFragment<AuthorizationContract.VPContract.Presenter, AuthorizationViewModel, AuthorizationDtoRequest>
        implements AuthorizationContract.VPContract.View {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = AuthorizationFragment.class.getSimpleName();

    private FragmentAuthorizationBinding fragmentBinding;
    private AuthorizationViewModel authorizationViewModel;
    private AuthorizationContract.ViewListener viewListener;

    public AuthorizationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);

        if (context instanceof AuthorizationContract.ViewListener) {
            viewListener = (AuthorizationContract.ViewListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement AuthorizationContract.ViewListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewListener = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        authorizationViewModel = new AuthorizationViewModel();

        fragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_authorization, container, false);

        fragmentBinding.setAuthorizationViewModel(authorizationViewModel);

        return fragmentBinding.getRoot();
    }


    //region Implementation: AuthorizationContract.VPContract.View
    @Override
    public void onLoginError(String error) {
        fragmentBinding.textInputLayoutLoginAuthorizationFragment.setErrorEnabled(true);
        fragmentBinding.textInputLayoutLoginAuthorizationFragment.setError(error);
    }

    @Override
    public void onPasswordError(String error) {
        fragmentBinding.textInputLayoutPasswordAuthorizationFragment.setErrorEnabled(true);
        fragmentBinding.textInputLayoutPasswordAuthorizationFragment.setError(error);
    }

    @Override
    public void onSuccessAuthorization() {
        viewListener.onSuccessAuthorization();
        showProgress(false);
    }

    @Override
    public void onFailureAuthorization() {
        viewListener.onFailureAuthorization();
        showProgress(false);
    }
    //endregion

    //region Implementation: LockableFragment
    @Override
    protected void request(@NonNull AuthorizationDtoRequest requestItem) {
        presenter.signIn(requestItem);
    }

    @Override
    protected void hideAllErrors() {
        fragmentBinding.textInputLayoutLoginAuthorizationFragment.setErrorEnabled(false);
        fragmentBinding.textInputLayoutPasswordAuthorizationFragment.setErrorEnabled(false);
    }

    @Override
    protected void showProgress(boolean show) {
        if (show) {
            ViewUtils.closeKeyboard(getActivity());
            fragmentBinding.pbAuthorizationFragment.setVisibility(View.VISIBLE);
        } else {
            fragmentBinding.pbAuthorizationFragment.setVisibility(View.GONE);
        }

        ViewUtils.setViewGroupEnabled(fragmentBinding.authorizationFragment, !show, true);
    }

    @Override
    protected boolean viewModelHasBadFields(@NonNull AuthorizationViewModel viewModel) {
        Context context = getContext();

        if (context == null) {
            return false;
        }

        if (authorizationViewModel.getLogin() == null || authorizationViewModel.getLogin().length() == 0) {
            onErrorMessage(context.getString(R.string.authorization_fragment_login_cannot_be_null_text));
            fragmentBinding.etLoginAuthorizationFragment.requestFocus();
            fragmentBinding.svAuthorizationFragment.scrollTo(0, fragmentBinding.etLoginAuthorizationFragment.getTop());
            return true;
        }

        if (authorizationViewModel.getPassword() == null || authorizationViewModel.getPassword().length() == 0) {
            onErrorMessage(context.getString(R.string.authorization_fragment_password_cannot_be_null_text));
            fragmentBinding.etPasswordAuthorizationFragment.requestFocus();
            fragmentBinding.svAuthorizationFragment.scrollTo(0, fragmentBinding.etPasswordAuthorizationFragment.getTop());
            return true;
        }

        return false;
    }

    @Override
    protected AuthorizationDtoRequest convertViewModelToRequest(@NonNull AuthorizationViewModel viewModel) {
        return new AuthorizationDtoRequest(viewModel.getLogin(), viewModel.getPassword());
    }
    //endregion
}