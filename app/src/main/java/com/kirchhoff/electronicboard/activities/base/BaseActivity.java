package com.kirchhoff.electronicboard.activities.base;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseContract;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@SuppressLint("Registered")
public class BaseActivity
        extends AppCompatActivity {

    private List<WeakReference<Fragment>> activeFragments = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        activeFragments.add(new WeakReference<>(fragment));
    }

    protected List<Fragment> getActiveFragments() {
        ArrayList<Fragment> ret = new ArrayList<>();

        for (WeakReference<Fragment> ref : activeFragments) {
            Fragment fragment = ref.get();
            if (fragment != null) {
                if (fragment.isVisible()) {
                    ret.add(fragment);
                }
            }
        }

        return ret;
    }
}
