package com.kirchhoff.electronicboard.activities.login.authorization;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseRepository;
import com.kirchhoff.electronicboard.repository.service.ServiceCallHub;
import com.kirchhoff.electronicboard.repository.service.data.login.authorization.SignInRequest;

import javax.inject.Inject;

public class AuthorizationRepository
        extends BaseRepository<AuthorizationContract.IRContract.Interactor>
        implements AuthorizationContract.IRContract.Repository {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = AuthorizationRepository.class.getSimpleName();

    private ServiceCallHub serviceCallHub;

    @Inject
    AuthorizationRepository(ServiceCallHub serviceCallHub) {
        this.serviceCallHub = serviceCallHub;
    }

    //region Implementation: AuthorizationContract.IRContract.Repository
    @Override
    public void signIn(@NonNull SignInRequest request) {

    }
    //endregion
}