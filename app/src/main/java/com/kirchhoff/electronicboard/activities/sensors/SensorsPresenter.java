package com.kirchhoff.electronicboard.activities.sensors;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BasePresenter;

import javax.inject.Inject;

public class SensorsPresenter
        extends BasePresenter<SensorsContract.VPContract.View, SensorsContract.PIContract.Interactor>
        implements SensorsContract.VPContract.Presenter, SensorsContract.PIContract.Presenter {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = SensorsPresenter.class.getSimpleName();

    @Inject
    SensorsPresenter() {}

    //region Implementation: SensorsContract.VPContract.Presenter
    //endregion

    //region Implementation: SensorsContract.PIContract.Presenter
    //endregion
}
