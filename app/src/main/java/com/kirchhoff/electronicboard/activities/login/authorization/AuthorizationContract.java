package com.kirchhoff.electronicboard.activities.login.authorization;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseContract;
import com.kirchhoff.electronicboard.activities.login.authorization.data.AuthorizationDtoRequest;
import com.kirchhoff.electronicboard.repository.service.data.login.authorization.SignInRequest;

public interface AuthorizationContract {

    interface RootRequests<R> {
        void signIn(@NonNull R request);
    }

    interface RootResponses {
        void onLoginError(String error);
        void onPasswordError(String error);

        void onSuccessAuthorization();
        void onFailureAuthorization();
    }

    interface VPContract {
        interface View extends
                BaseContract.VPContract.View,
                RootResponses {}

        interface Presenter extends
                BaseContract.VPContract.Presenter,
                RootRequests<AuthorizationDtoRequest> {}
    }

    interface PIContract {
        interface Presenter extends
                BaseContract.PIContract.Presenter,
                RootResponses {}

        interface Interactor extends
                BaseContract.PIContract.Interactor,
                RootRequests<SignInRequest> {}
    }

    interface IRContract {
        interface Interactor extends
                BaseContract.IRContract.Interactor,
                RootResponses {}

        interface Repository extends
                BaseContract.IRContract.Repository,
                RootRequests<SignInRequest> {}
    }

    interface ViewListener {
        void onSuccessAuthorization();
        void onFailureAuthorization();
    }
}