package com.kirchhoff.electronicboard.activities;

import com.kirchhoff.electronicboard.activities.fan.FanFragment;
import com.kirchhoff.electronicboard.activities.fan.FanFragmentModule;
import com.kirchhoff.electronicboard.activities.lights.LightsFragment;
import com.kirchhoff.electronicboard.activities.lights.LightsFragmentModule;
import com.kirchhoff.electronicboard.activities.login.LoginActivity;
import com.kirchhoff.electronicboard.activities.login.LoginActivityModule;
import com.kirchhoff.electronicboard.activities.login.authorization.AuthorizationFragment;
import com.kirchhoff.electronicboard.activities.login.authorization.AuthorizationFragmentModule;
import com.kirchhoff.electronicboard.activities.login.registration.RegistrationFragment;
import com.kirchhoff.electronicboard.activities.login.registration.RegistrationFragmentModule;
import com.kirchhoff.electronicboard.activities.main.MainActivity;
import com.kirchhoff.electronicboard.activities.main.MainActivityModule;
import com.kirchhoff.electronicboard.activities.scopes.ActivityScope;
import com.kirchhoff.electronicboard.activities.scopes.FragmentScope;
import com.kirchhoff.electronicboard.activities.sensors.SensorsFragment;
import com.kirchhoff.electronicboard.activities.sensors.SensorsFragmentModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = AndroidSupportInjectionModule.class)
public interface BindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {LoginActivityModule.class})
    LoginActivity loginActivityInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = {AuthorizationFragmentModule.class})
    AuthorizationFragment authorizationFragmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = {RegistrationFragmentModule.class})
    RegistrationFragment registrationFragmentInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    MainActivity mainActivityInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = {FanFragmentModule.class})
    FanFragment fanFragmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = {LightsFragmentModule.class})
    LightsFragment lightsFragmentInjector();

    @FragmentScope
    @ContributesAndroidInjector(modules = {SensorsFragmentModule.class})
    SensorsFragment sensorsFragmentInjector();
}
