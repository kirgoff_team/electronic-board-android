package com.kirchhoff.electronicboard.activities.sensors;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kirchhoff.electronicboard.R;
import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseViewFragment;
import com.kirchhoff.electronicboard.databinding.FragmentSensorsBinding;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class SensorsFragment
        extends BaseViewFragment<SensorsContract.VPContract.Presenter>
        implements SensorsContract.VPContract.View {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = SensorsFragment.class.getSimpleName();

    @SuppressWarnings("FieldCanBeLocal")
    private FragmentSensorsBinding fragmentBinding;

    private SensorsContract.ViewListener viewListener;

    public SensorsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_sensors, container, false);

        return fragmentBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);

        if (context instanceof SensorsContract.ViewListener) {
            viewListener = (SensorsContract.ViewListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement SensorsContract.ViewListener");
        }
    }

    @Override
    public void onDetach() {
        viewListener = null;
        super.onDetach();
    }
}