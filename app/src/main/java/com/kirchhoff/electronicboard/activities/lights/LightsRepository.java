package com.kirchhoff.electronicboard.activities.lights;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseRepository;
import com.kirchhoff.electronicboard.repository.service.ServiceCallHub;

import javax.inject.Inject;

public class LightsRepository
        extends BaseRepository<LightsContract.IRContract.Interactor>
        implements LightsContract.IRContract.Repository {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = LightsRepository.class.getSimpleName();

    private ServiceCallHub serviceCallHub;

    @Inject
    LightsRepository(ServiceCallHub serviceCallHub) {
        this.serviceCallHub = serviceCallHub;
    }

    //region Implementation: LightsContract.IRContract.Repository
    //endregion
}
