package com.kirchhoff.electronicboard.activities.base.architecture_components;

import android.annotation.SuppressLint;
import android.widget.Toast;

import javax.inject.Inject;
import com.kirchhoff.electronicboard.activities.base.BaseActivity;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;

@SuppressLint("Registered")
public class BaseViewActivity<P extends BaseContract.VPContract.Presenter>
        extends BaseActivity
        implements BaseContract.VPContract.View {

    protected P presenter;

    @Inject
    public void injectPresenter(P presenter) {
        this.presenter = presenter;
        presenter.setView(this);
    }

    @Override
    protected void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }

    //region Implementation: BaseContract.VPContract.View
    @Override
    public void onErrorMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorInvalidRequestPayload() {
        onErrorMessage("Invalid Request Payload");
    }

    @Override
    public void onErrorBadToken() {
        onErrorMessage("Bad Token");
    }

    @Override
    public void onErrorPermissionDenied() {
        onErrorMessage("Permission Denied");
    }

    @Override
    public void onErrorRequestingUserNotFound() {
        onErrorMessage("Requesting User Not Found");
    }

    @Override
    public void onErrorInternalServerError() {
        onErrorMessage("Internal Server Error");
    }
    //endregion
}