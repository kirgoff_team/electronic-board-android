package com.kirchhoff.electronicboard.activities.sensors;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseContract;

public interface SensorsContract {
    interface RootRequests {
    }

    interface RootResponses {
    }

    interface VPContract {
        interface View extends
                BaseContract.VPContract.View,
                RootResponses {}

        interface Presenter extends
                BaseContract.VPContract.Presenter,
                RootRequests {}
    }

    interface PIContract {
        interface Presenter extends
                BaseContract.PIContract.Presenter,
                RootResponses {}

        interface Interactor extends
                BaseContract.PIContract.Interactor,
                RootRequests {}
    }

    interface IRContract {
        interface Interactor extends
                BaseContract.IRContract.Interactor,
                RootResponses {}

        interface Repository extends
                BaseContract.IRContract.Repository,
                RootRequests {}
    }

    interface ViewListener {
    }
}
