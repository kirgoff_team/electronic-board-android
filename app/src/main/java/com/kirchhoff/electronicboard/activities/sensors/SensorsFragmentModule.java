package com.kirchhoff.electronicboard.activities.sensors;

import com.kirchhoff.electronicboard.activities.scopes.FragmentScope;

import dagger.Binds;
import dagger.Module;

@Module
public interface SensorsFragmentModule {

    @Binds
    @FragmentScope
    SensorsContract.VPContract.View provideSensorsContractVPContractView(SensorsFragment view);

    @Binds
    @FragmentScope
    SensorsContract.VPContract.Presenter provideSensorsContractVPContractPresenter(SensorsPresenter presenter);

    @Binds
    @FragmentScope
    SensorsContract.PIContract.Presenter provideSensorsContractPIContractPresenter(SensorsPresenter presenter);

    @Binds
    @FragmentScope
    SensorsContract.PIContract.Interactor provideSensorsContractPIContractInteractor(SensorsInteractor interactor);

    @Binds
    @FragmentScope
    SensorsContract.IRContract.Interactor provideSensorsContractIRContractInteractor(SensorsInteractor interactor);

    @Binds
    @FragmentScope
    SensorsContract.IRContract.Repository provideSensorsContractIRContractRepository(SensorsRepository repository);
}