package com.kirchhoff.electronicboard.activities.fan;

import com.kirchhoff.electronicboard.activities.scopes.FragmentScope;

import dagger.Binds;
import dagger.Module;

@Module
public interface FanFragmentModule {

    @Binds
    @FragmentScope
    FanContract.VPContract.View provideFanContractVPContractView(FanFragment view);

    @Binds
    @FragmentScope
    FanContract.VPContract.Presenter provideFanContractVPContractPresenter(FanPresenter presenter);

    @Binds
    @FragmentScope
    FanContract.PIContract.Presenter provideFanContractPIContractPresenter(FanPresenter presenter);

    @Binds
    @FragmentScope
    FanContract.PIContract.Interactor provideFanContractPIContractInteractor(FanInteractor interactor);

    @Binds
    @FragmentScope
    FanContract.IRContract.Interactor provideFanContractIRContractInteractor(FanInteractor interactor);

    @Binds
    @FragmentScope
    FanContract.IRContract.Repository provideFanContractIRContractRepository(FanRepository repository);
}