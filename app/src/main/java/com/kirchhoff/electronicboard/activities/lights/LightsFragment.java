package com.kirchhoff.electronicboard.activities.lights;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.kirchhoff.electronicboard.R;
import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseViewFragment;
import com.kirchhoff.electronicboard.databinding.FragmentLightsBinding;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class LightsFragment
        extends BaseViewFragment<LightsContract.VPContract.Presenter>
        implements LightsContract.VPContract.View {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = LightsFragment.class.getSimpleName();

    private LightsContract.ViewListener viewListener;
    private FragmentLightsBinding fragmentBinding;

    public LightsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_lights, container, false);

        fragmentBinding.fragmentLightsLightBulb.tbLightBulb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    fragmentBinding.fragmentLightsLightBulb.sbLightBulb.setEnabled(true);
                } else {
                    fragmentBinding.fragmentLightsLightBulb.sbLightBulb.setEnabled(false);
                }
            }
        });

        return fragmentBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);

        if (context instanceof LightsContract.ViewListener) {
            viewListener = (LightsContract.ViewListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement LightsContract.ViewListener");
        }
    }

    @Override
    public void onDetach() {
        viewListener = null;
        super.onDetach();
    }
}
