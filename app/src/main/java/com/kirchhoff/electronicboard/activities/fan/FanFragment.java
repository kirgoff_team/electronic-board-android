package com.kirchhoff.electronicboard.activities.fan;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kirchhoff.electronicboard.R;
import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseViewFragment;
import com.kirchhoff.electronicboard.databinding.FragmentFanBinding;

import dagger.android.support.AndroidSupportInjection;

public class FanFragment
        extends BaseViewFragment<FanContract.VPContract.Presenter>
        implements FanContract.VPContract.View {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = FanFragment.class.getSimpleName();

    private FanContract.ViewListener viewListener;
    private FragmentFanBinding fragmentBinding;

    public FanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_fan, container, false);

        fragmentBinding.fragmentFanExtractorFan.tbExtractorFan.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                fragmentBinding.fragmentFanExtractorFan.sbExtractorFanPower.setEnabled(true);
            } else {
                fragmentBinding.fragmentFanExtractorFan.sbExtractorFanPower.setEnabled(false);
            }
        });

        return fragmentBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);

        if (context instanceof FanContract.ViewListener) {
            viewListener = (FanContract.ViewListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FanContract.ViewListener");
        }
    }

    @Override
    public void onDetach() {
        viewListener = null;
        super.onDetach();
    }
}