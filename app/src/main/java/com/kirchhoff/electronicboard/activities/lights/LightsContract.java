package com.kirchhoff.electronicboard.activities.lights;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseContract;
import com.kirchhoff.electronicboard.activities.fan.FanContract;

public interface LightsContract {
    interface RootRequests {
    }

    interface RootResponses {
    }

    interface VPContract {
        interface View extends
                BaseContract.VPContract.View,
                FanContract.RootResponses {}

        interface Presenter extends
                BaseContract.VPContract.Presenter,
                FanContract.RootRequests {}
    }

    interface PIContract {
        interface Presenter extends
                BaseContract.PIContract.Presenter,
                FanContract.RootResponses {}

        interface Interactor extends
                BaseContract.PIContract.Interactor,
                FanContract.RootRequests {}
    }

    interface IRContract {
        interface Interactor extends
                BaseContract.IRContract.Interactor,
                FanContract.RootResponses {}

        interface Repository extends
                BaseContract.IRContract.Repository,
                FanContract.RootRequests {}
    }

    interface ViewListener {
    }
}
