package com.kirchhoff.electronicboard.activities.login.registration;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BasePresenter;
import com.kirchhoff.electronicboard.activities.login.registration.data.RegistrationDtoRequest;
import com.kirchhoff.electronicboard.repository.service.data.login.registration.SignUpRequest;

import javax.inject.Inject;

public class RegistrationPresenter
        extends BasePresenter<RegistrationContract.VPContract.View, RegistrationContract.PIContract.Interactor>
        implements RegistrationContract.VPContract.Presenter, RegistrationContract.PIContract.Presenter {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = RegistrationPresenter.class.getSimpleName();

    @Inject
    RegistrationPresenter() {}

    //region Implementation: RegistrationContract.VPContract.Presenter
    @Override
    public void signUp(@NonNull RegistrationDtoRequest request) {
        interactor.signUp(new SignUpRequest(request.login, request.password));
    }
    //endregion

    //region Implementation: RegistrationContract.PIContract.Presenter
    @Override
    public void onLoginError(String error) {
        view.onLoginError(error);
    }

    @Override
    public void onPasswordError(String error) {
        view.onPasswordError(error);
    }

    @Override
    public void onSuccessRegistration() {
        view.onSuccessRegistration();
    }

    @Override
    public void onFailureRegistration() {
        view.onFailureRegistration();
    }
    //endregion
}