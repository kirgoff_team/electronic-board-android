package com.kirchhoff.electronicboard.activities.fan;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BasePresenter;

import javax.inject.Inject;

public class FanPresenter
        extends BasePresenter<FanContract.VPContract.View, FanContract.PIContract.Interactor>
        implements FanContract.VPContract.Presenter, FanContract.PIContract.Presenter {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = FanPresenter.class.getSimpleName();

    @Inject
    FanPresenter() {}

    //region Implementation: FanContract.VPContract.Presenter
    //endregion

    //region Implementation: FanContract.PIContract.Presenter
    //endregion
}