package com.kirchhoff.electronicboard.activities.base.architecture_components;

public interface BaseContract {

    interface VPContract {
        interface View extends CommonErrors {
        }

        interface Presenter {
            void setView(View view);
            void onDestroy();
        }
    }

    interface PIContract {
        interface Presenter extends CommonErrors {
        }

        interface Interactor {
            void setPresenter(Presenter presenter);
            void onDestroy();
        }
    }

    interface IRContract {
        interface Interactor extends CommonErrors {
        }

        interface Repository {
            void setInteractor(Interactor interactor);
            void onDestroy();
        }
    }

    interface CommonErrors {
        void onErrorMessage(String error);
        void onErrorInvalidRequestPayload();
        void onErrorBadToken();
        void onErrorPermissionDenied();
        void onErrorRequestingUserNotFound();
        void onErrorInternalServerError();
    }
}