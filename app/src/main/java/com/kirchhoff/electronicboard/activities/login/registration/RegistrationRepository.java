package com.kirchhoff.electronicboard.activities.login.registration;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseRepository;
import com.kirchhoff.electronicboard.repository.authorization_token.AuthToken;
import com.kirchhoff.electronicboard.repository.service.InterceptedSingleObserver;
import com.kirchhoff.electronicboard.repository.service.RetrofitClient;
import com.kirchhoff.electronicboard.repository.service.ServiceCallHub;
import com.kirchhoff.electronicboard.repository.service.data.base.BaseResponseError;
import com.kirchhoff.electronicboard.repository.service.data.login.registration.SignUpRequest;
import com.kirchhoff.electronicboard.repository.service.data.login.registration.SignUpResponse;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class RegistrationRepository
        extends BaseRepository<RegistrationContract.IRContract.Interactor>
        implements RegistrationContract.IRContract.Repository {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = RegistrationRepository.class.getSimpleName();
    private static final String SIGN_UP_REQUEST_TAG = "SignUp Request -> ";

    private AuthToken authToken;
    private ServiceCallHub serviceCallHub;
    private RetrofitClient retrofitClient;

    @Inject
    RegistrationRepository(AuthToken authToken, ServiceCallHub serviceCallHub, RetrofitClient retrofitClient) {
        this.authToken = authToken;
        this.serviceCallHub = serviceCallHub;
        this.retrofitClient = retrofitClient;
    }

    //region Implementation: RegistrationContract.IRContract.Repository
    @Override
    public void signUp(final @NonNull SignUpRequest request) {
        disposables.add(
                serviceCallHub.signUp(request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new InterceptedSingleObserver<SignUpResponse>(
                                SIGN_UP_REQUEST_TAG,
                                interactor,
                                retrofitClient) {

                            @Override
                            public void onFailureRequest(@NonNull HttpException httpException, @NonNull BaseResponseError responseError) {
                                signUpOnFailure(httpException, responseError);
                            }

                            @Override
                            public void onRequestDoneUnsuccessfully() {
                                interactor.onFailureRegistration();
                            }

                            @Override
                            public void onRequestDoneSuccessfully(@NonNull SignUpResponse signUpResponse) {
                                signUpOnResponse(signUpResponse);
                            }
                        }));
    }
    //endregion

    private void signUpOnResponse(@NonNull SignUpResponse signUpResponse) {
        authToken.setToken(signUpResponse.jwt);
        interactor.onSuccessRegistration();
    }

    private void signUpOnFailure(@NonNull HttpException httpException, @NonNull BaseResponseError responseError) {

        int httpErrorCode = httpException.code();

        switch (httpErrorCode) {
            //region Error 422
            case 422: {
                switch (responseError.code) {
                    case 106:
                        interactor.onLoginError(responseError.error);
                        break;
                    case 107:
                        interactor.onLoginError(responseError.error);
                        break;
                    case 108:
                        interactor.onPasswordError(responseError.error);
                        break;
                    default:
                        interactor.onErrorMessage(SIGN_UP_REQUEST_TAG + "Unknown error code: " + responseError.code);
                        break;
                }

                break;
            }
            //endregion

            default:
                interactor.onErrorMessage(SIGN_UP_REQUEST_TAG + "Unknown http error code: " + httpErrorCode);
                break;
        }
    }
}