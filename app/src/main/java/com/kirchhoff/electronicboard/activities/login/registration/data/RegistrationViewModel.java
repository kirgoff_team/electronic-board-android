package com.kirchhoff.electronicboard.activities.login.registration.data;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

public class RegistrationViewModel extends BaseObservable {
    private String regLogin;
    private String regPassword;
    private String regRepeatedPassword;

    public RegistrationViewModel() {}

    public RegistrationViewModel(String login, String password, String repeatedPassword) {
        this.regLogin = login;
        this.regPassword = password;
        this.regRepeatedPassword = repeatedPassword;
    }

    @Bindable
    public String getLogin() {
        return regLogin;
    }

    @Bindable
    public String getPassword() {
        return regPassword;
    }

    @Bindable
    public String getRepeatedPassword() {
        return regRepeatedPassword;
    }

    public void setLogin(String login) {
        this.regLogin = login;
        notifyPropertyChanged(BR.login);
    }

    public void setPassword(String password) {
        this.regPassword = password;
        notifyPropertyChanged(BR.password);
    }

    public void setRepeatedPassword(String password) {
        this.regRepeatedPassword = password;
        notifyPropertyChanged(BR.repeatedPassword);
    }

    @Override
    public String toString() {
        return "RegistrationViewModel{" +
                "regLogin='" + regLogin + '\'' +
                ", regPassword='" + regPassword + '\'' +
                ", regRepeatedPassword='" + regPassword + '\'' +
                '}';
    }
}