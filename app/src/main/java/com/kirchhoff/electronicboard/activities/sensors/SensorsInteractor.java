package com.kirchhoff.electronicboard.activities.sensors;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseInteractor;

import javax.inject.Inject;

public class SensorsInteractor
        extends BaseInteractor<SensorsContract.IRContract.Repository, SensorsContract.PIContract.Presenter>
        implements SensorsContract.IRContract.Interactor, SensorsContract.PIContract.Interactor {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = SensorsInteractor.class.getSimpleName();

    @Inject
    SensorsInteractor() {}

    //region Implementation: SensorsContract.PIContract.Interactor
    //endregion

    // region Implementation: SensorsContract.IRContract.Interactor
    //endregion
}