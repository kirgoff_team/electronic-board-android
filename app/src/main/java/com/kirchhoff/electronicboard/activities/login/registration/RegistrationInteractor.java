package com.kirchhoff.electronicboard.activities.login.registration;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseInteractor;
import com.kirchhoff.electronicboard.repository.service.data.login.registration.SignUpRequest;

import javax.inject.Inject;

public class RegistrationInteractor
        extends BaseInteractor<RegistrationContract.IRContract.Repository, RegistrationContract.PIContract.Presenter>
        implements RegistrationContract.IRContract.Interactor, RegistrationContract.PIContract.Interactor {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = RegistrationInteractor.class.getSimpleName();

    @Inject
    RegistrationInteractor() {}

    //region Implementation: RegistrationContract.PIContract.Interactor
    @Override
    public void signUp(@NonNull SignUpRequest request) {
        repository.signUp(request);
    }
    //endregion

    //region Implementation: RegistrationContract.IRContract.Interactor
    @Override
    public void onLoginError(String error) {
        presenter.onLoginError(error);
    }

    @Override
    public void onPasswordError(String error) {
        presenter.onPasswordError(error);
    }

    @Override
    public void onSuccessRegistration() {
        presenter.onSuccessRegistration();
    }

    @Override
    public void onFailureRegistration() {
        presenter.onFailureRegistration();
    }
    //endregion
}