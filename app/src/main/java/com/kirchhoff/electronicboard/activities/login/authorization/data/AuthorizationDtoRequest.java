package com.kirchhoff.electronicboard.activities.login.authorization.data;

public class AuthorizationDtoRequest {

    public String login;
    public String password;

    public AuthorizationDtoRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }
}