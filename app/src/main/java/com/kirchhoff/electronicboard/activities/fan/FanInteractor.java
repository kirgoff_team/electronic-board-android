package com.kirchhoff.electronicboard.activities.fan;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseInteractor;

import javax.inject.Inject;

public class FanInteractor
        extends BaseInteractor<FanContract.IRContract.Repository, FanContract.PIContract.Presenter>
        implements FanContract.IRContract.Interactor, FanContract.PIContract.Interactor {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = FanInteractor.class.getSimpleName();

    @Inject
    FanInteractor() {}

    //region Implementation: FanContract.PIContract.Interactor
    //endregion

    // region Implementation: FanContract.IRContract.Interactor
    //endregion
}
