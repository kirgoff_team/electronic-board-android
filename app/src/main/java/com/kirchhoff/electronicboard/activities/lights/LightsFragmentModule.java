package com.kirchhoff.electronicboard.activities.lights;

import com.kirchhoff.electronicboard.activities.scopes.FragmentScope;

import dagger.Binds;
import dagger.Module;

@Module
public interface LightsFragmentModule {

    @Binds
    @FragmentScope
    LightsContract.VPContract.View provideLightsContractVPContractView(LightsFragment view);

    @Binds
    @FragmentScope
    LightsContract.VPContract.Presenter provideLightsContractVPContractPresenter(LightsPresenter presenter);

    @Binds
    @FragmentScope
    LightsContract.PIContract.Presenter provideLightsContractPIContractPresenter(LightsPresenter presenter);

    @Binds
    @FragmentScope
    LightsContract.PIContract.Interactor provideLightsContractPIContractInteractor(LightsInteractor interactor);

    @Binds
    @FragmentScope
    LightsContract.IRContract.Interactor provideLightsContractIRContractInteractor(LightsInteractor interactor);

    @Binds
    @FragmentScope
    LightsContract.IRContract.Repository provideLightsContractIRContractRepository(LightsRepository repository);
}