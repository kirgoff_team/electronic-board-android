package com.kirchhoff.electronicboard.activities.lights;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BasePresenter;

import javax.inject.Inject;

public class LightsPresenter
        extends BasePresenter<LightsContract.VPContract.View, LightsContract.PIContract.Interactor>
        implements LightsContract.VPContract.Presenter, LightsContract.PIContract.Presenter {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = LightsPresenter.class.getSimpleName();

    @Inject
    LightsPresenter() {}

    //region Implementation: LightsContract.VPContract.Presenter
    //endregion

    //region Implementation: LightsContract.PIContract.Presenter
    //endregion
}
