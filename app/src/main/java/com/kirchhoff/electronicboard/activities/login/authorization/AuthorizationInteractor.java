package com.kirchhoff.electronicboard.activities.login.authorization;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseInteractor;
import com.kirchhoff.electronicboard.repository.service.data.login.authorization.SignInRequest;

import javax.inject.Inject;

public class AuthorizationInteractor
        extends BaseInteractor<AuthorizationContract.IRContract.Repository, AuthorizationContract.PIContract.Presenter>
        implements AuthorizationContract.IRContract.Interactor, AuthorizationContract.PIContract.Interactor {


    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = AuthorizationInteractor.class.getSimpleName();

    @Inject
    AuthorizationInteractor() {}

    //region Implementation: AuthorizationContract.PIContract.Interactor
    @Override
    public void signIn(@NonNull SignInRequest request) {

    }
    //endregion

    //region Implementation: AuthorizationContract.IRContract.Interactor
    @Override
    public void onLoginError(String error) {

    }

    @Override
    public void onPasswordError(String error) {

    }

    @Override
    public void onSuccessAuthorization() {

    }

    @Override
    public void onFailureAuthorization() {

    }
    //endregion
}