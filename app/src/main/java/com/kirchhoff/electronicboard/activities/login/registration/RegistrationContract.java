package com.kirchhoff.electronicboard.activities.login.registration;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseContract;
import com.kirchhoff.electronicboard.activities.login.registration.data.RegistrationDtoRequest;
import com.kirchhoff.electronicboard.repository.service.data.login.registration.SignUpRequest;


public interface RegistrationContract {
    interface RootRequests<R> {
        void signUp(@NonNull R request);
    }

    interface RootResponses {
        void onLoginError(String error);
        void onPasswordError(String error);

        void onSuccessRegistration();
        void onFailureRegistration();
    }

    interface VPContract {
        interface View extends
                BaseContract.VPContract.View,
                RootResponses {}

        interface Presenter extends
                BaseContract.VPContract.Presenter,
                RootRequests<RegistrationDtoRequest> {}
    }

    interface PIContract {
        interface Presenter extends
                BaseContract.PIContract.Presenter,
                RootResponses {}

        interface Interactor extends
                BaseContract.PIContract.Interactor,
                RootRequests<SignUpRequest> {}
    }

    interface IRContract {
        interface Interactor extends
                BaseContract.IRContract.Interactor,
                RootResponses {}

        interface Repository extends
                BaseContract.IRContract.Repository,
                RootRequests<SignUpRequest> {}
    }

    interface ViewListener {
        void onSuccessRegistration();
        void onFailureRegistration();
    }
}
