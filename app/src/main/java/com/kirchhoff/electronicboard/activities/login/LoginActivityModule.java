package com.kirchhoff.electronicboard.activities.login;

import com.kirchhoff.electronicboard.activities.scopes.ActivityScope;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginActivityModule {

    @Provides
    @ActivityScope
    NavigationRouter injectNavigationRouter(LoginActivity activity) {
        return new NavigationRouter(activity);
    }
}