package com.kirchhoff.electronicboard.activities.login.registration;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kirchhoff.electronicboard.R;
import com.kirchhoff.electronicboard.activities.base.LockableFragment;
import com.kirchhoff.electronicboard.activities.login.registration.data.RegistrationDtoRequest;
import com.kirchhoff.electronicboard.activities.login.registration.data.RegistrationViewModel;
import com.kirchhoff.electronicboard.activities.main.MainActivity;
import com.kirchhoff.electronicboard.databinding.FragmentRegistrationBinding;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;
import com.kirchhoff.electronicboard.utilities.ViewUtils;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class RegistrationFragment
        extends LockableFragment<RegistrationContract.VPContract.Presenter, RegistrationViewModel, RegistrationDtoRequest>
        implements RegistrationContract.VPContract.View {

    @SuppressWarnings({"UnusedDeclaration"})
    private static final String TAG = RegistrationFragment.class.getSimpleName();

    private FragmentRegistrationBinding fragmentBinding;
    private RegistrationViewModel registrationViewModel;
    private RegistrationContract.ViewListener viewListener;

    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);

        if (context instanceof RegistrationContract.ViewListener) {
            viewListener = (RegistrationContract.ViewListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement RegistrationContract.ViewListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        viewListener = null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        registrationViewModel = new RegistrationViewModel();

        fragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_registration, container, false);

        fragmentBinding.setRegistrationViewModel(registrationViewModel);
        fragmentBinding.bRegistrationContinue.setOnClickListener(v -> onSuccessRegistration());
//        fragmentBinding.bRegistrationContinue.setOnClickListener(v -> sendRequest(registrationViewModel));

        return fragmentBinding.getRoot();
    }

    //region Implementation: RegistrationContract.VPContract.View
    @Override
    public void onLoginError(String error) {
        fragmentBinding.textInputLayoutLoginRegistrationFragment.setErrorEnabled(true);
        fragmentBinding.textInputLayoutLoginRegistrationFragment.setError(error);
    }

    @Override
    public void onPasswordError(String error) {
        fragmentBinding.textInputLayoutPasswordRegistrationFragment.setErrorEnabled(true);
        fragmentBinding.textInputLayoutPasswordRegistrationFragment.setError(error);
    }

    @Override
    public void onSuccessRegistration() {
        viewListener.onSuccessRegistration();
        showProgress(false);
    }

    @Override
    public void onFailureRegistration() {
        viewListener.onFailureRegistration();
        showProgress(false);
    }
    //endregion

    public void onRepeatedPasswordError(String error) {
        fragmentBinding.textInputLayoutRepeatedPasswordRegistrationFragment.setErrorEnabled(true);
        fragmentBinding.textInputLayoutRepeatedPasswordRegistrationFragment.setError(error);
    }

    //region Implementation: LockableFragment
    @Override
    protected void request(@NonNull RegistrationDtoRequest requestItem) {
        presenter.signUp(requestItem);
    }

    @Override
    protected void hideAllErrors() {
        fragmentBinding.textInputLayoutLoginRegistrationFragment.setErrorEnabled(false);
        fragmentBinding.textInputLayoutPasswordRegistrationFragment.setErrorEnabled(false);
        fragmentBinding.textInputLayoutRepeatedPasswordRegistrationFragment.setErrorEnabled(false);
    }

    @Override
    protected void showProgress(boolean show) {
        if (show) {
            ViewUtils.closeKeyboard(getActivity());
            fragmentBinding.pbRegistrationFragment.setVisibility(View.VISIBLE);
        } else {
            fragmentBinding.pbRegistrationFragment.setVisibility(View.GONE);
        }

        ViewUtils.setViewGroupEnabled(fragmentBinding.registrationFragment, !show, true);
    }

    @Override
    protected boolean viewModelHasBadFields(@NonNull RegistrationViewModel viewModel) {
        Context context = getContext();

        if (context == null) {
            return false;
        }

        String login = registrationViewModel.getLogin();
        String password = registrationViewModel.getPassword();
        String repeatedPassword = registrationViewModel.getRepeatedPassword();

        if (login == null || login.length() == 0) {
            onLoginError(context.getString(R.string.registration_fragment_login_cannot_be_null_text));
            fragmentBinding.etLoginRegistrationFragment.requestFocus();
            fragmentBinding.svRegistrationFragment.scrollTo(0, fragmentBinding.etLoginRegistrationFragment.getTop());
            return true;
        }

        if (password == null || password.length() == 0) {
            onPasswordError(context.getString(R.string.registration_fragment_password_cannot_be_null_text));
            fragmentBinding.etPasswordRegistrationFragment.requestFocus();
            fragmentBinding.svRegistrationFragment.scrollTo(0, fragmentBinding.etPasswordRegistrationFragment.getTop());
            return true;
        }

        if (!password.equals(repeatedPassword)) {
            onRepeatedPasswordError(context.getString(R.string.registration_fragment_passwords_do_not_match_text));
            fragmentBinding.etRepeatedPasswordRegistrationFragment.requestFocus();
            fragmentBinding.svRegistrationFragment.scrollTo(0, fragmentBinding.etRepeatedPasswordRegistrationFragment.getTop());
            return true;
        }

        return false;
    }

    @Override
    protected RegistrationDtoRequest convertViewModelToRequest(@NonNull RegistrationViewModel viewModel) {
        return new RegistrationDtoRequest(viewModel.getLogin(), viewModel.getPassword());
    }
    //endregion
}