package com.kirchhoff.electronicboard.repository.service;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.activities.base.architecture_components.BaseContract;
import com.kirchhoff.electronicboard.repository.service.data.base.BaseResponseError;
import com.kirchhoff.electronicboard.repository.service.data.login.registration.SignUpResponse;

import java.lang.ref.WeakReference;

import io.reactivex.observers.DisposableSingleObserver;
import retrofit2.HttpException;

public abstract class InterceptedSingleObserver<T> extends DisposableSingleObserver<T> {
    private final String REQUEST_TAG;
    private final WeakReference<BaseContract.IRContract.Interactor> interactorReference;
    private final WeakReference<RetrofitClient> retrofitClientReference;

    @SuppressWarnings("WeakerAccess")
    public InterceptedSingleObserver(String requestTag, BaseContract.IRContract.Interactor interactor, RetrofitClient retrofitClient) {
        this.REQUEST_TAG = requestTag;
        this.interactorReference = new WeakReference<>(interactor);
        this.retrofitClientReference = new WeakReference<>(retrofitClient);
    }

    @Override
    public final void onSuccess(T t) {
        if (t == null) {
            interactorReference.get().onErrorMessage(
                    REQUEST_TAG  + SignUpResponse.class.getSimpleName() + " is null");
            onRequestDoneUnsuccessfully();
        } else {
            onRequestDoneSuccessfully(t);
        }
    }

    @Override
    public final void onError(Throwable error) {
        if (!(error instanceof HttpException)) {
            interactorReference.get().onErrorMessage(REQUEST_TAG + "onFailure: " + error.getMessage());
            onRequestDoneUnsuccessfully();
            return;
        }

        final HttpException httpException = (HttpException) error;
        final int httpErrorCode = httpException.code();
        final BaseResponseError responseError  =
                retrofitClientReference.get().convertToError(BaseResponseError.class, httpException.response());

        if (responseError == null) {
            interactorReference.get().onErrorMessage(
                    REQUEST_TAG + BaseResponseError.class.getSimpleName() +
                            " is null. Response code: " + httpErrorCode);
            onRequestDoneUnsuccessfully();
            return;
        }

        switch (httpErrorCode) {
            //region Error 400
            case 400:
                if (responseError.code == 100) {
                    interactorReference.get().onErrorInvalidRequestPayload();
                }
                break;
            //endregion
            //region Error 401
            case 401:
                if (responseError.code == 102) {
                    interactorReference.get().onErrorBadToken();
                }
                break;
            //endregion
            //region Error 403
            case 403:
                if (responseError.code == 103) {
                    interactorReference.get().onErrorPermissionDenied();
                }
                break;
            //endregion
            //region Error 404
            case 404:
                if (responseError.code == 105) {
                    interactorReference.get().onErrorRequestingUserNotFound();
                }
                break;
            //endregion
            //region Error 500
            case 500:
                if (responseError.code == 110) {
                    interactorReference.get().onErrorInternalServerError();
                }
                break;
            //endregion
        }

        onFailure(httpException, responseError);
    }

    private void onFailure(@NonNull HttpException httpException, @NonNull BaseResponseError responseError) {
        onFailureRequest(httpException, responseError);
        onRequestDoneUnsuccessfully();
    }

    public abstract void onFailureRequest(@NonNull HttpException httpException, @NonNull BaseResponseError responseError);
    public abstract void onRequestDoneUnsuccessfully();
    public abstract void onRequestDoneSuccessfully(@NonNull T t);
}
