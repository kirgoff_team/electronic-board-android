package com.kirchhoff.electronicboard.repository.service.data.common;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public enum UserType {
    @SerializedName("1")
    @Expose
    NEWBIE,

    @SerializedName("2")
    @Expose
    READER,

    @SerializedName("4")
    @Expose
    ADMIN;

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new Gson().toJson(this);
    }
}