package com.kirchhoff.electronicboard.repository.service.data;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProtoDataResponse {
    @SerializedName("data")
    @Expose
    public String data;

    @Override
    public String toString() {
        return ProtoDataResponse.class.getSimpleName() + new Gson().toJson(this);
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static String toJson(ProtoDataResponse protoDataResponse) {
        return new Gson().toJson(protoDataResponse);
    }

    public static ProtoDataResponse fromJson(String protoDataResponseJson) {
        return new Gson().fromJson(protoDataResponseJson, ProtoDataResponse.class);
    }
}
