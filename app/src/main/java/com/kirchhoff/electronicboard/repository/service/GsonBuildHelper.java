package com.kirchhoff.electronicboard.repository.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("WeakerAccess")
public class GsonBuildHelper {
    public static Gson buildGson() {
        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }
}
