package com.kirchhoff.electronicboard.repository.service.interceptors;

import android.support.annotation.NonNull;

import com.kirchhoff.electronicboard.repository.service.data.base.BaseResponseError;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ErrorsInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        final Request request = chain.request();
        final okhttp3.Response response = chain.proceed(request);
        final int responseCode = response.code();

//        BaseResponseError responseError  =
//                retrofitClient.convertToError(BaseResponseError.class, response);

        switch (responseCode) {
            case (400):
                // Invalid request payload
                break;
            case (401):
                // Bad token
                break;
            case (403):
                // Permission denied
                break;
            case (404):
                // Requesting user not found
                break;
            case (500):
                // Internal server error
                break;
        }
            return response;
//        }
    }
}