package com.kirchhoff.electronicboard.repository.authorization_token;

public interface AuthToken {

    String getToken();

    void setToken(String token);

    void removeToken();

    boolean tokenIsEmpty();
}
