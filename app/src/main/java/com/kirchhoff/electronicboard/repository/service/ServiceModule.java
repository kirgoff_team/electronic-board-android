package com.kirchhoff.electronicboard.repository.service;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.kirchhoff.electronicboard.repository.authorization_token.AuthToken;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public class ServiceModule {

    @Provides
    @Singleton
    OkHttpClient okHttpClient(Application app, AuthToken authToken) {
        return new HttpClient(app, authToken).getOkHttpClient();
    }

    @Singleton
    @Provides
    RetrofitClient provideRetrofitClient(OkHttpClient okHttpClient, Gson gson) {
        return new RetrofitClient(okHttpClient, gson);
    }

    @Singleton
    @Provides
    ServiceCallHub provideServiceCallHub(RetrofitClient client) {
        return client.getRetrofit().create(ServiceCallHub.class);
    }

    @Singleton
    @Provides
    Gson provideGson() {
        return GsonBuildHelper.buildGson();
    }
}