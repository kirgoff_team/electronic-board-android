package com.kirchhoff.electronicboard.repository.authorization_token;

public class AuthTokenInMemoryImpl implements AuthToken {

    private static final String EMPTY_TOKEN = "";
    private String token = EMPTY_TOKEN;

    public AuthTokenInMemoryImpl(String token) {
        this.token = token;
    }

    public AuthTokenInMemoryImpl() {
        this(EMPTY_TOKEN);
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public void removeToken() {
        token = EMPTY_TOKEN;
    }

    @Override
    public boolean tokenIsEmpty() {
        return token.equals(EMPTY_TOKEN);
    }
}
