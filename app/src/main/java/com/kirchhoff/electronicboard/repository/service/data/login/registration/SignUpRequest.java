package com.kirchhoff.electronicboard.repository.service.data.login.registration;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignUpRequest {
    @SerializedName("login")
    @Expose
    public String login;

    @SerializedName("password")
    @Expose
    public String password;

    public SignUpRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new Gson().toJson(this);
    }
}