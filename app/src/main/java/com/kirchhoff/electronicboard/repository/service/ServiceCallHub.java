package com.kirchhoff.electronicboard.repository.service;

import com.kirchhoff.electronicboard.repository.service.data.ProtoDataResponse;
import com.kirchhoff.electronicboard.repository.service.data.login.authorization.SignInRequest;
import com.kirchhoff.electronicboard.repository.service.data.login.authorization.SignInResponse;
import com.kirchhoff.electronicboard.repository.service.data.login.registration.SignUpRequest;
import com.kirchhoff.electronicboard.repository.service.data.login.registration.SignUpResponse;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServiceCallHub {

    //region User
    @POST("api_v1/user/sign_up")
    Single<SignUpResponse> signUp(@Body SignUpRequest request);

    @POST("api_v1/user/sign_in")
    Single<SignInResponse> signIn(@Body SignInRequest request);
    //endregion

    @GET("api_v1/data/hello")
    Single<ProtoDataResponse> hello();
}