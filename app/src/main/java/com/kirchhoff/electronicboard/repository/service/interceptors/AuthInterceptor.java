package com.kirchhoff.electronicboard.repository.service.interceptors;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kirchhoff.electronicboard.repository.authorization_token.AuthToken;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {

    private AuthToken authToken;

    public AuthInterceptor(@Nullable AuthToken authToken) {
        this.authToken = authToken;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request originalRequest = chain.request();

        if (authToken == null || authToken.tokenIsEmpty()) {
            return chain.proceed(originalRequest);
        }

        Request.Builder requestBuilder = originalRequest.newBuilder()
                .header("authorization", authToken.getToken());

        Request changedRequest = requestBuilder.build();

        return chain.proceed(changedRequest);
    }
}