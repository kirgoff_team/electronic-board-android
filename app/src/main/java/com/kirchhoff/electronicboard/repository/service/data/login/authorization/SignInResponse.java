package com.kirchhoff.electronicboard.repository.service.data.login.authorization;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kirchhoff.electronicboard.repository.service.data.common.FullUserInfo;
import com.kirchhoff.electronicboard.repository.service.data.common.UserType;

public class SignInResponse {
    @SerializedName("user")
    @Expose
    public FullUserInfo user;

    @SerializedName("jwt")
    @Expose
    public String jwt;

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new Gson().toJson(this);
    }
}