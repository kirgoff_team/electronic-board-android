package com.kirchhoff.electronicboard.repository.service.data.base;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponseError {
    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("error")
    @Expose
    public String error;

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new Gson().toJson(this);
    }
}