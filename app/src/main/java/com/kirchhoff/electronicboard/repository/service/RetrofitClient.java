package com.kirchhoff.electronicboard.repository.service;

import com.google.gson.Gson;

import java.lang.annotation.Annotation;
import javax.annotation.Nullable;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private Retrofit retrofit;
    private static final String BASE_URL = "https://192.168.31.165:3443";

    public RetrofitClient(OkHttpClient okHttpClient, Gson gson) {
        this(okHttpClient, gson, BASE_URL);
    }

    public RetrofitClient(OkHttpClient okHttpClient, Gson gson, String baseUrl) {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    @Nullable
    public <R, RE> RE convertToError(Class errorClassName, @Nullable Response<R> response) {
        if (response == null) {
            return null;
        }

        ResponseBody responseErrorBody = response.errorBody();

        if (responseErrorBody == null) {
            return null;
        }

        Converter<ResponseBody, RE> converter =
                retrofit.responseBodyConverter(errorClassName, new Annotation[0]);

        try {
            return converter.convert(responseErrorBody);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}