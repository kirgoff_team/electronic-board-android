package com.kirchhoff.electronicboard.repository.authorization_token;

import android.content.SharedPreferences;

public class AuthTokenImpl implements AuthToken {

    private static final String EMPTY_TOKEN = "";
    private static final String AUTH_TOKEN_PREFERENCES_KEY = "AuthToken";

    private SharedPreferences preferences;

    AuthTokenImpl(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public String getToken() {
        return preferences.getString(AUTH_TOKEN_PREFERENCES_KEY, EMPTY_TOKEN);
    }

    @Override
    public void setToken(String token) {
        preferences.edit().putString(AUTH_TOKEN_PREFERENCES_KEY, token).apply();
    }

    @Override
    public void removeToken() {
        preferences.edit().remove(AUTH_TOKEN_PREFERENCES_KEY).apply();
    }

    @Override
    public boolean tokenIsEmpty() {
        return getToken().equals(EMPTY_TOKEN);
    }
}
