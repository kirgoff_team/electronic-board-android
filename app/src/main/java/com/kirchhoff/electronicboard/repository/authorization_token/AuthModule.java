package com.kirchhoff.electronicboard.repository.authorization_token;

import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AuthModule {

    @Singleton
    @Provides
    AuthToken provideAuthToken(SharedPreferences preferences) {
        return new AuthTokenImpl(preferences);
    }
}