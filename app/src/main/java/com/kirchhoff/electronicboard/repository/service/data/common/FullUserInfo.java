package com.kirchhoff.electronicboard.repository.service.data.common;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FullUserInfo {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("login")
    @Expose
    public String login;

    @SerializedName("user_type")
    @Expose
    public UserType userType;

    @SerializedName("must_change_password")
    @Expose
    public boolean mustChangePassword;

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + new Gson().toJson(this);
    }
}