package com.kirchhoff.electronicboard.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

public class ViewUtils {
    public static void setViewGroupEnabled(ViewGroup layout, boolean enabled, boolean includeNestedViewGroups) {
        layout.setEnabled(enabled);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (includeNestedViewGroups && child instanceof ViewGroup) {
                setViewGroupEnabled((ViewGroup) child, enabled, true);
            } else {
                child.setEnabled(enabled);
            }
        }
    }

    public static void closeKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }

        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();

        if (inputManager != null && view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
    }
}
