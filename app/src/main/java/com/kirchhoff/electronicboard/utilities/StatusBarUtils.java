package com.kirchhoff.electronicboard.utilities;

import android.app.Activity;
import android.os.Build;
import android.view.WindowManager;

public class StatusBarUtils {
    /**
     * Устанавливает либо снимает полупрозрачность со статус-бара. Работает только для версий
     * SDK >= LOLLIPOP, для версия младше LOLLIPOP, метод ничего не делает.
     * @param activity активити в которой производится работа над статус-баром
     * @param makeTranslucent true - для установки полупрозрачности, false - для снятия полупрозрачности
     */
    public static void setStatusBarTranslucent(Activity activity, boolean makeTranslucent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (makeTranslucent) {
                activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            } else {
                activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
    }
}
