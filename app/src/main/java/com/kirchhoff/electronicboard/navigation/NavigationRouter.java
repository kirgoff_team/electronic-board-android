package com.kirchhoff.electronicboard.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

public class NavigationRouter {
    private WeakReference<Context> contextWeakReference;

    public NavigationRouter(@NonNull Context context) {
        this.contextWeakReference = new WeakReference<>(context);
    }

    public boolean navigateTo(@NonNull Class className, @Nullable Bundle bundle, @Nullable Integer flags) {
        if (contextWeakReference == null || contextWeakReference.get() == null) {
            return false;
        }

        Intent intent = new Intent(contextWeakReference.get(), className);
        if (bundle != null) {
            intent.putExtras(bundle);
        }

        if (flags != null) {
            intent.addFlags(flags);
        }

        contextWeakReference.get().startActivity(intent);
        return true;
    }

    public boolean navigateTo(@NonNull Class className, @Nullable Bundle bundle) {
        return navigateTo(className, bundle, null);
    }

    public boolean navigateTo(@NonNull Class className) {
        return navigateTo(className, null);
    }

    public boolean navigateForResult(@NonNull Activity activity, @NonNull Class className, int requestCode, @Nullable Bundle bundle) {
        if (contextWeakReference == null || contextWeakReference.get() == null) {
            return false;
        }

        Intent intent = new Intent(contextWeakReference.get(), className);

        activity.startActivityForResult(intent, requestCode, bundle);
        return true;
    }

    public boolean navigateForResult(@NonNull Activity activity, @NonNull Class className, int requestCode) {
        return navigateForResult(activity, className, requestCode, null);
    }
}
