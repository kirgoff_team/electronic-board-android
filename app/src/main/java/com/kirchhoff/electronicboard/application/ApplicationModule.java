package com.kirchhoff.electronicboard.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.kirchhoff.electronicboard.application.base.BaseApplicationClass;
import com.kirchhoff.electronicboard.navigation.NavigationRouter;
import com.squareup.leakcanary.RefWatcher;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@SuppressWarnings("WeakerAccess")
@Module
public class ApplicationModule {
    private Application app;
    private SharedPreferences sharedPreferences;

    public ApplicationModule(Application app){
        this.app = app;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(app);
    }

    @Singleton
    @Provides
    Application provideApplication(){
        return app;
    }

    @Singleton
    @Provides
    SharedPreferences provideDefaultSharedPreferences(){
        return sharedPreferences;
    }

    @Singleton
    @Provides
    RefWatcher provideLeakCanaryRefWatcher() {
        BaseApplicationClass application = (BaseApplicationClass) app;
        return application.getLeakCanaryRefWatcher();
    }
}
