package com.kirchhoff.electronicboard.application;

import com.kirchhoff.electronicboard.activities.BindingModule;
import com.kirchhoff.electronicboard.application.base.BaseApplicationComponent;
import com.kirchhoff.electronicboard.repository.authorization_token.AuthModule;
import com.kirchhoff.electronicboard.repository.service.ServiceModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                ServiceModule.class,
                AuthModule.class,
                BindingModule.class
        })
abstract class ApplicationComponent implements BaseApplicationComponent {

    @Component.Builder
    public interface Builder {

        Builder applicationModule(ApplicationModule applicationModule);
        Builder serviceModule(ServiceModule serviceModule);
        Builder authModule(AuthModule authModule);

        BaseApplicationComponent build();
    }
}