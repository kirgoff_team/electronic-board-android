package com.kirchhoff.electronicboard.application.base;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.support.HasSupportFragmentInjector;

public abstract class BaseApplicationClass extends Application
        implements HasActivityInjector, HasSupportFragmentInjector {

    protected BaseApplicationComponent applicationComponent;
    private RefWatcher leakCanaryRefWatcher;

    @Inject
    protected DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Inject
    protected DispatchingAndroidInjector<Fragment> dispatchingFragmentInjector;

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingFragmentInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        leakCanaryRefWatcher = LeakCanary.install(this);

        initApplicationComponent();
    }

    public RefWatcher getLeakCanaryRefWatcher() {
        return leakCanaryRefWatcher;
    }

    protected abstract BaseApplicationComponent initApplicationComponent();
}