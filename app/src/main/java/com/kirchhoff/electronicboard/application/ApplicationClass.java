package com.kirchhoff.electronicboard.application;

import android.support.v7.app.AppCompatDelegate;

import com.kirchhoff.electronicboard.application.base.BaseApplicationClass;
import com.kirchhoff.electronicboard.application.base.BaseApplicationComponent;
import com.kirchhoff.electronicboard.repository.authorization_token.AuthModule;
import com.kirchhoff.electronicboard.repository.service.ServiceModule;

public class ApplicationClass extends BaseApplicationClass {

    @Override
    public void onCreate() {
        super.onCreate();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected BaseApplicationComponent initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .serviceModule(new ServiceModule())
                .authModule(new AuthModule())
                .build();

        applicationComponent.inject(this);

        return applicationComponent;
    }
}